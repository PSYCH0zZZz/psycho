package com.example.config;

import io.micronaut.context.annotation.ConfigurationProperties;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@ConfigurationProperties("ms.cat")
public class CatConfig {
    String host;
    int port;
}
