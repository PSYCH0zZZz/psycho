package com.example.config;

import io.micronaut.context.annotation.ConfigurationProperties;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@ConfigurationProperties("ms.dog")
public class DogConfig {
    String host;
    int port;
}
