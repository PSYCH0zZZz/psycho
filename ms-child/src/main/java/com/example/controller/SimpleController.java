package com.example.controller;

import com.example.config.CatConfig;
import com.example.config.DogConfig;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Controller
@Slf4j
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SimpleController {
    static String WHOAMI_PATTERN = "http://%s:%d/whoami";
    static String BASE_PATTERN = "http://%s:%d";
    HttpClient client = HttpClient.newBuilder()
            .build();
    CatConfig catConfig;
    DogConfig dogConfig;


    @Get(processes = MediaType.TEXT_PLAIN)
    public String getHello() throws IOException, InterruptedException {
        StringBuilder result = new StringBuilder();
        fillCat(result);
        fillDog(result);

        return result.toString();
    }

    private void fillCat(StringBuilder result) throws IOException, InterruptedException {
        result.append(client.send(
                HttpRequest.newBuilder()
                        .uri(URI.create(String.format(WHOAMI_PATTERN, catConfig.getHost(), catConfig.getPort())))
                        .build(), HttpResponse.BodyHandlers.ofString()
        ).body().toString());
        result.append(" says ");
        result.append(client.send(
                HttpRequest.newBuilder()
                        .uri(URI.create(String.format(BASE_PATTERN, catConfig.getHost(), catConfig.getPort())))
                        .build(), HttpResponse.BodyHandlers.ofString()
        ).body().toString());
        result.append("\n");
    }

    private void fillDog(StringBuilder result) throws IOException, InterruptedException {
        result.append(client.send(
                HttpRequest.newBuilder()
                        .uri(URI.create(String.format(WHOAMI_PATTERN, dogConfig.getHost(), dogConfig.getPort())))
                        .build(), HttpResponse.BodyHandlers.ofString()
        ).body().toString());
        result.append(" says ");
        result.append(client.send(
                HttpRequest.newBuilder()
                        .uri(URI.create(String.format(BASE_PATTERN, dogConfig.getHost(), dogConfig.getPort())))
                        .build(), HttpResponse.BodyHandlers.ofString()
        ).body().toString());
        result.append("\n");
    }

    @PostConstruct
    public void showInfo() {
        log.info("Host: {}, Port: {} for cat", catConfig.getHost(), catConfig.getPort());
        log.info("Host: {}, Port: {} for dog", dogConfig.getHost(), dogConfig.getPort());
    }
}
